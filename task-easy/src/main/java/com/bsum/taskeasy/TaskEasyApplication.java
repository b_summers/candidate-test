package com.bsum.taskeasy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@SpringBootApplication
public class TaskEasyApplication {

	public static void main(String[] args) {
		if (!(new File("src/main/resources/static/data/academy_award_actresses.json").exists())) {
			convertCSVToJson("src/main/resources/static/data/academy_award_actresses.csv");
		}
		SpringApplication.run(TaskEasyApplication.class, args);
	}

	private static void convertCSVToJson(String csvFile) {

		try (BufferedReader in = new BufferedReader(new FileReader(csvFile))) {
		    List <Movie> movies = in.lines().skip(1).map(line -> {
		        String[] info = line.split(",");
		        return new Movie(info[0], info[1], info[2]);
		    }).collect(Collectors.toList());
		    ObjectMapper mapper = new ObjectMapper();
		    mapper.enable(SerializationFeature.INDENT_OUTPUT);
		    mapper.writeValue(new File("src/main/resources/static/data/academy_award_actresses.json"), movies);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
