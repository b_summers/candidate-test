package com.bsum.taskeasy;

public class Movie {
	
	public String title;
	public String actress;
	public String yearReleased;
	
	public Movie(String yearReleased, String actress, String title) {
		this.title = title;
		this.actress = actress;
		this.yearReleased = yearReleased;
	}

}
